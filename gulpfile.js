'use strict';

import gulp, * as gulpCli from 'gulp';
// import ('gulp-sass'); // (require('sass')); // (require('sass'))
// import ('browser-sync');
// import del from 'del';

import gulpImagemin, * as imagemin from "./node_modules/gulp-imagemin/index.js";

// import ('gulp-uglify');
// import ('gulp-usemin');
// import ('gulp-rev');
// let cleanCss = require('gulp-clean-css');
// import ('gulp-flatmap');
// import ('gulp-htmlmin');


// let gulp = require('gulp');
// let sass = require('gulp-sass')(require('sass')); // (require('sass'))
// let browserSync = require('browser-sync').create();
// let del = require('del');
// let imagemin = require('gulp-imagemin');
// let uglify = require('gulp-uglify');
// let usemin = require('gulp-usemin');
// let rev = require('gulp-rev');
// let cleanCss = require('gulp-clean-css');
// let flatmap = require('gulp-flatmap');
// let htmlmin = require('gulp-htmlmin');


gulp.task('sass', function(){
  return gulp.src('./css/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./css'));
})

gulp.task('sass:watch', function(){
  gulp.watch('./css/*.scss', ['sass']);
});

gulp.task('serve', gulp.series( 'sass', function(){
  var files = ['./*.html', './css/*.css', './img/*.{png, jpg, gif}', './js/*.js']
  browserSync.init(files, {
    server: {
      baseDir: './'
    }
  });
}));

gulp.task('default', gulp.series('serve') );

// gulp.start('sass:watch');

gulp.task('clean', function(){
  return del(['dist']);
});

gulp.task('copyfonts', function(){
  gulp.src('./node_modules/open-iconic/font/fonts/*.*')
    .pipe(gulp.dest('./dist/fonts'));
})

gulp.task('imagemin', function(){
  return gulp.src('./images/*.{png,jpg,jpeg,gif}')
    .pipe(imagemin({optimizationLevel: 3, progressive: true, interlaced: true}))
    .pipe(gulp.dest('dist/images'));
});

gulp.task('usemin', function(){
  return gulp.src('./*.html')
    .pipe(flatmap(function(stream, file){
      return stream
       .pipe(usemin({
         css: [rev()],
         html: [function(){return htmlmin({collapseWhitespace: true})}],
         js: [uglify(), rev()],
         inlinejs: [uglifyjs()],
         inlinecss: [cleanCss(), 'concat']
       }));
    }))
    .pipe(gulp.dest('dist/'));

});

gulp.task('build', gulp.series('clean','copyfonts','imagemin','usemin'));

// gulp.task('build', ['clean'], function(){
//   gulp.start('copyfonts', 'imagemin', 'usemin');
// });
