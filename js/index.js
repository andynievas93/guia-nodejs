$(function(){
    $('[data-bs-toggle="tooltip"]').Tooltip();
    $('[data-bs-toggle="popover"]').popover();
    // Velocidad del carousel
    $('.carousel').carousel({
        interval: 2000
    });
});

$('#modal-formulario-register').on('show.bs.modal', (e)=>{
  console.log("Modal comienza a abrirse");
  $('#btn-register').removeClass('btn-success');
  $('#btn-register').addClass('btn-outline-dark');
  $('#btn-register').prop('disabled', true);
});

$('#modal-formulario-register').on('shown.bs.modal', (e)=>{
  console.log("Modal termina de abrirse");
});

$('#modal-formulario-register').on('hide.bs.modal', (e)=>{
  console.log("Modal comienza a ocultarse");
});

$('#modal-formulario-register').on('hidden.bs.modal', (e)=>{
  console.log("Modal ocultado");
  $('#btn-register').removeClass('btn-outline-dark');
  $('#btn-register').addClass('btn-success');
  $('#btn-register').prop('disabled', false);
});
